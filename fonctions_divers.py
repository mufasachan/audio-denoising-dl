from FeatureExtractor import FeatureExtractor
import numpy as np

def decoupage(data, *, numSegmentsInput,  windowLength, overlap, sample_rate, numFeatures):
    """
        Fonction de découpage des données. On donne en entrée un ensemble d'audio en ndarray.
        On récupère en sortie un tableau découpé des modules carrée de spectogramme par paquet
        de [numFeatures, numSegmentsInput]

        Parameters :
        data (list) : Liste des audios venant de load_database()
        numSegmentsInput (int) : Nombre de spectre par paquet
        window_length (int) : Largeur de la fenêtre pour le spectogramme
        overlap (float) : Pourcentage de recouvrement entre les segments
        sample_rate (int) : La fréquence d'échantillonnage
        numFeatures (int) : Longueur fréquentiel des spectres du spectogramme 

        Return :
        data_d_magn (list de np.array [numFeatures, numSegmentsInput]) : Les modules carrées des
        spectogramme découpés.
    """
    ENORME_ET_SEC = 50000
    data_d_magn = np.zeros([ENORME_ET_SEC, numFeatures, numSegmentsInput])
    for i,son in enumerate(data):
        # Extraction des features (Module carrée)
        extractor = FeatureExtractor(son, windowLength=windowLength, 
        overlap=overlap, sample_rate=sample_rate)
        stft = extractor.get_stft_spectrogram()
        magn, _ = extractor.get_magn_phase_from_stft(stft)
        # Découpage des module carrée
        for j in range(0, magn.shape[1], numSegmentsInput):
            data_d_magn[i,:,:] = magn[:,j:(j+numSegmentsInput)]
    
    print("Avec zero : {}".format(data_d_magn.shape))
    print("Sans zero : {}".format(data_d_magn[ data_d_magn != 0].shape))
    return data_d_magn[ data_d_magn != 0] # On enlève les valeurs nuls