# Projet Deep learning

Fichier `readme` du projet, reprenant les différents aspects du projet rapidement.

## Arborescence du projet

* . (racine) : Possède les scripts `.py` utilisés par le notebook, le notebook de test pour l'écriture de test [brouillon](./brouillon.ipynb) et le notebook de process qui fait tout le traitement [notebook](./notebook.ipynb).
* [data](./data/) : Le dossier comprenant les données que nous avons besoins pour entrainer notre réseau de neurones
  * [babble.wav](./data/babble.wav) : Le fichier bruit de cafétariat utilisé pour créer les audios bruités.
  * [data/test/](./data/test/) : Le dossier contenant les [sons bruitées](./data/test/predictor/) et les [sons claires](./data/test/target/) de test.
  * [data/train/](./data/train/) : Le dossier contenant les [sons bruitées](./data/train/predictor/) et les [sons claires](./data/train/target/) d'entrainement.
