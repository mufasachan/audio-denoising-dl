import scipy
import librosa
import numpy as np

# Crédit : https://github.com/daitan-innovation/cnn-audio-denoiser
class FeatureExtractor:
    """
        Classe permettant de manipuler les spectrogrames des signaux audios
    Class variables :
    audio (np.array [nAudio]) : L'audio au quel nous allons extraire le spectro
    ffT_length (int) : Largeur de la fenêtre pour le spectogramme
    window_length (int) : Largeur de la fenêtre pour le spectogramme
    overlap (float) : Pourcentage de recouvrement entre les segments
    sample_rate (int) : La fréquence d'échantillonnage
    window (np.array [window_length]) : La fenêtre d'apodisation utilisé pour le spectro
    """
    def __init__(self, audio, *, windowLength, overlap, sample_rate):
        """
            Buider de la classe
        Parameters : 
        audio (np.array [nAudio]) : L'audio au quel nous allons extraire le spectro
        window_length (int) : Largeur de la fenêtre pour le spectogramme
        overlap (float) : Pourcentage de recouvrement entre les segments
        sample_rate (int) : La fréquence d'échantillonnage
        """
        self.audio = audio
        self.ffT_length = windowLength
        self.window_length = windowLength
        self.overlap = overlap
        self.sample_rate = sample_rate
        self.window = scipy.signal.hamming(self.window_length, sym=False)

    def get_stft_spectrogram(self):
        """
            Donne la spectrogramme de l'audio
            Returns value (np.array [ffT_length, *]) = spectrogramme
        """
        return librosa.stft(self.audio, n_fft=self.ffT_length, win_length=self.window_length, hop_length=self.overlap,
                            window=self.window, center=True)

    def get_audio_from_stft_spectrogram(self, stft_features):
        """
            Donne l'audio à partir du spectro
            Parameter : 
            stft_features (np.array [ffT_length, *]): stft
            Return value (np.array [*]) : audio
        """
        return librosa.istft(stft_features, win_length=self.window_length, hop_length=self.overlap,
                             window=self.window, center=True)
    def get_magn_phase_from_stft(self, stft):
        """
            Donne le module carré et la phase de la stft
            Parameter : 
            stft (np.array [ffT_length, *]) : Le spectogramme
            Return values : 
            magn (np.array [ffT_length, *]) : Le module carré
            phase (np.array [ffT_length, *]) : La phase

        """

        magn = np.abs(stft)**2
        phase = np.angle(stft)
        return magn, phase
    def get_mel_spectrogram(self):
        return librosa.feature.melspectrogram(self.audio, sr=self.sample_rate, power=2.0, pad_mode='reflect',
                                           n_fft=self.ffT_length, hop_length=self.overlap, center=True)

    def get_audio_from_mel_spectrogram(self, M):
        return librosa.feature.inverse.mel_to_audio(M, sr=self.sample_rate, n_fft=self.ffT_length, hop_length=self.overlap,
                                             win_length=self.window_length, window=self.window,
                                             center=True, pad_mode='reflect', power=2.0, n_iter=32, length=None)