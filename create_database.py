import os
import numpy as np
import librosa

def create_database():
    """
    Créer la base de donnée, affiche si la base de données est existante ou pas
    """
    path = os.getcwd() + '/data/'
    if not os.listdir(path + 'train/predictor/'): # Si sons bruitées non existants
        print("Création de la base de données")
        sampleRate = 8000
        # Import audio clean
        # Train
        print("Bruit importé")
        pathTrain = path + 'train/target/'
        dirTrain = os.listdir(pathTrain)
        sonTrain = []
        for fileAudio in dirTrain:
            son,_ = librosa.load(pathTrain + fileAudio, sr=sampleRate)
            sonTrain.append(son)
        # Test
        pathTest = path + 'test/target/'
        dirTest = os.listdir(pathTest)
        sonTest = []
        for fileAudio in dirTest:
            son,_ = librosa.load(pathTest + fileAudio, sr=sampleRate)
            sonTest.append(son)
        sonClean = sonTrain + sonTest
        # Import le bruit
        bruit, sr = librosa.load(path + '/babble.wav', sr=sampleRate)
        # Bruitage
        puissance = lambda x :  1 / len(x) * np.sum([i**2 for i in x])
        RSB = 0
        sonBruite = []
        for son in sonClean:
            # Découpe du bruit
            longueurSon = len(son)
            instantRandom = random.randint(0, len(bruit) - longueurSon-1)
            b =  bruit[instantRandom:instantRandom+longueurSon]
            # Appliquer le RSP
            puissanceB = puissance(b)
            puissanceSon = puissance(son)
            puissanceBPrime = puissanceSon / (np.power(10, RSB / 20) )
            b = np.sqrt(puissanceBPrime / puissanceB) * b
            print("Puissance b :{}, Puissance son : {}".format(puissance(b), puissanceSon))
            # Bruitage
            sonBruite.append(son + b)
        sonBruiteTrain = sonBruite[:len(sonTrain)]
        sonBruiteTest = sonBruite[len(sonTrain):]
        pathPTrain = path + '/train/predictor/'
        pathPTest = path + '/test/predictor/'
        for i,nameFile in enumerate(dirTrain):
            write(pathPTrain + nameFile, sampleRate, sonBruiteTrain[i])
        for i,nameFile in enumerate(dirTest):
            write(pathPTest + nameFile, sampleRate, sonBruiteTest[i])
        print("Base de données crée")
    else : 
        print("Base de données existante")

