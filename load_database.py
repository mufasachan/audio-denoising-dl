import os
import soundfile as sf
def load_database():
    """
    Charge la base de données.

    Returns values :
    -------------------------------------------------
    x_train : predicteur d'entrainement (list)
    x_test : predicteur de test (list)
    y_train : cible d'entrainement (list)
    y_test : cible de test (list)
    sampleRate : fréquence d'échantillonage (int)
    """
    path = os.getcwd() + '/data/'
    x_train, x_test, y_train, y_test = [],[],[],[]
    pathTrainP = path + '/train/predictor/'
    pathTrainT = path + '/train/target/'
    pathTestP = path + '/test/predictor/'
    pathTestT = path + '/test/target/'
    _,sampleRate = sf.read(pathTrainP + os.listdir(pathTrainP)[0])
    for dirFile in os.listdir(pathTrainP):
        son,_ = sf.read(pathTrainP + dirFile)
        x_train.append(son)
    for dirFile in os.listdir(pathTestP):
        son,_ = sf.read(pathTestP + dirFile)
        x_test.append(son)
    for dirFile in os.listdir(pathTrainT):
        son,_ = sf.read(pathTrainT + dirFile)
        y_train.append(son)
    for dirFile in os.listdir(pathTestT):
        son,_ = sf.read(pathTestT + dirFile)
        y_test.append(son)
    return x_train, x_test, y_train, y_test, sampleRate
